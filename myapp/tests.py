from django.test import TestCase, Client
from myapp.models import Status
from django.urls import resolve
from .views import home
from .forms import StatusForm




class Story6Test(TestCase):
    def test_urls_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_view_use_correct_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_lab_3_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_inside_html(self):
        response=Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello World", response_content)

    def test_model(self):
        Status.objects.create(status ="Yo Wassap")
        counting_all_variable_activity = Status.objects.all().count()
        self.assertEqual(counting_all_variable_activity, 1)

    def test_form_validation_blank_items(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    
    def test_validation_form_form_filled_items(self):
        response = self.client.post('', data={'status':"Status"})   
        response_content = response.content.decode()
        self.assertIn(response_content, "Status")
    



    
































# from django.test import TestCase
# from myapp.models import Status


# # models test
# class WhateverTest(TestCase):

#     def create_whatever(self, title="only a test", body="yes, this is only a test"):
#         return Status.objects.create(status=status)

#     def test_whatever_creation(self):
#         w = self.create_whatever()
#         self.assertTrue(isinstance(w, Status))
#         self.assertEqual(w.__unicode__(), w.title)
