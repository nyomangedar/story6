from django import forms
from .models import Status
from django.forms import ModelForm


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = '__all__'


