from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm

# Create your views here.
def home(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
        return redirect('/')
    else:
        status = StatusForm()
        data = Status.objects.all()
        response = {'Data':data, 'status': status}
        return render(request, 'home.html', response)




    # status = Status()
    # status.StatusForm = form.cleaned_data['status']
    #         data_item = status.save(commit=False)
    #         data_item.save()
    #         return render (request, 'home.html')
    # else:
    #     
    #     return render (request, 'home.html', {'status' : status})
    
    # 
    # context = {'Data': data}
    # return render(request, "home.html", context)